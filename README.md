# Pastebin Crawler
this is a small crawler for pastebin.com

## Run in docker:
* run ```docker run -it --rm moro256/intsights_web_crawler:latest```

## results location:
crawl results will be written to a tinyDB file located in the container at: ```/usr/src/app/intsights_web_crawler/db/db.json```

## requirements:
* python3

## configure
* configuration file is located in ```./intsights_web_crawler/config/config.json```