import json


class Paste:
    def __init__(self, id, title, author, content, date):
        self.id = id
        self.title = title
        self.author = author
        self.content = content
        self.date = date

    def __str__(self):
        jsonStr = json.dumps(self.__dict__, default=lambda o: o.__str__())
        return jsonStr
