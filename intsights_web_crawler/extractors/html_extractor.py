import intsights_web_crawler.services.page_retriever as page_retriever


class HtmlExtractor:
    """the HtmlExtractor is an abstract extractor.
    it takes a url as an argument, fetches it and provides the html page string to the deriving extractor
    Args:
        url: a http url
    """
    def __init__(self, url):
        self.url = url

    def extract(self):
        page_str = page_retriever.get_text(self.url)
        return self.extract_from_page_str(page_str)

    def extract_from_page_str(self, page_str):
        """when implemented, this function should extract data from the page string and return it
        Args:
            page_str: string of html page
        Returns:
            object representing data extracted from the page
        """
        raise NotImplementedError
