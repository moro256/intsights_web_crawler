from concurrent.futures import ThreadPoolExecutor, as_completed


class ConcurrentExtractor:
    """ConcurrentExtractor will execute multiple extractors in a parallel manner
    Args:
        max_workers: maximum number of threads to use
        extractors: a list of extractors to run in paralel
    """
    def __init__(self, max_workers, extractors):
        self.max_workers = max_workers
        self.extractors = extractors

    def extract(self):
        """will execute all extractors provided in the constructor in a parallel manner
        returns:
            iterator of results from all extractors
        """
        futures = []
        with ThreadPoolExecutor(max_workers=self.max_workers) as executor:
            for extractor in self.extractors:
                futures.append(executor.submit(extractor.extract))

        for future in as_completed(futures):
            yield future.result()
