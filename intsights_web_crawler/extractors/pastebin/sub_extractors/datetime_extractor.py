from pytz import timezone
from dateutil.parser import parse as parse_datetime
import arrow


class DateTimeExtractor:
    def __init__(self, pyquery_page_obj):
        self.pyquery_page_obj = pyquery_page_obj

    def extract(self):
        time_str = self.pyquery_page_obj("div.paste_box_line2 span[title]").eq(0).attr["title"]
        datetime = parse_datetime(time_str, tzinfos={"CDT": timezone('US/Central')})
        return arrow.get(datetime).to('utc')
