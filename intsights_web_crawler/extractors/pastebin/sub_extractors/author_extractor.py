class AuthorExtractor:
    def __init__(self, pyquery_page_obj):
        self.pyquery_page_obj = pyquery_page_obj

    def extract(self):
        return self.pyquery_page_obj("div.paste_box_line2 a").eq(0).text()
