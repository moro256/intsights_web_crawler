class ContentExtractor:
    def __init__(self, pyquery_page_obj):
        self.pyquery_page_obj = pyquery_page_obj

    def extract(self):
        return self.pyquery_page_obj("div.textarea_border textarea").eq(0).text().strip()
