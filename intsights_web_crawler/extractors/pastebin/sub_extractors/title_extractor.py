class TitleExtractor:
    def __init__(self, pyquery_page_obj):
        self.pyquery_page_obj = pyquery_page_obj

    def extract(self):
        return self.pyquery_page_obj("div.paste_box_info h1").eq(0).text()
