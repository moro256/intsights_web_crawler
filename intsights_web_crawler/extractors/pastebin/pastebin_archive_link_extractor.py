from pyquery import PyQuery
from intsights_web_crawler.extractors.html_extractor import HtmlExtractor


class PastebinArchiveLinkExtractor(HtmlExtractor):
    def __init__(self, pastebin_url="http://pastebin.com", pastes_page="/archive"):
        super().__init__(pastebin_url + pastes_page)
        self.pastebin_url = pastebin_url

    def extract_from_page_str(self, page_str):
        page_obj = PyQuery(page_str)
        for paste_row in page_obj('tr'):
            link = self.get_link_from_row(paste_row)
            if link is not None:
                yield self.pastebin_url + link

    def get_link_from_row(self, row):
        row_obj = PyQuery(row)
        return row_obj('td a[href]').eq(0).attr['href']
