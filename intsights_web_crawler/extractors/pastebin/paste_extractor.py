from pyquery import PyQuery
from intsights_web_crawler.data_structures.paste import Paste
from intsights_web_crawler.extractors.html_extractor import HtmlExtractor
from intsights_web_crawler.extractors.pastebin.sub_extractors.author_extractor import AuthorExtractor
from intsights_web_crawler.extractors.pastebin.sub_extractors.title_extractor import TitleExtractor
from intsights_web_crawler.extractors.pastebin.sub_extractors.content_extractor import ContentExtractor
from intsights_web_crawler.extractors.pastebin.sub_extractors.datetime_extractor import DateTimeExtractor


class PasteExtractor(HtmlExtractor):
    def __init__(self, paste_link):
        super().__init__(paste_link)

    def extract_from_page_str(self, page_str):
        paste_page_obj = PyQuery(page_str)
        title = TitleExtractor(paste_page_obj).extract()
        author = AuthorExtractor(paste_page_obj).extract()
        content = ContentExtractor(paste_page_obj).extract()
        time = DateTimeExtractor(paste_page_obj).extract()
        id = self.url.split('/')[-1]

        return Paste(id, title, author, content, time)
