from intsights_web_crawler.logger.base_logger import BaseLogger
import datetime


class ConsoleLogger(BaseLogger):
    def log(self, msg, level):
        print("{time} {level}: {msg}".format(time=datetime.datetime.now(), level=level, msg=msg))
