import requests


def get_text(url):
    return get_response(url).text


def get_response(url):
    response = requests.get(url)
    if response.status_code == 200:
        return response
    elif response.status_code == 404:
        raise Exception('url not found.')
    else:
        raise Exception('http get error: status_code: ' + str(response.status_code))
