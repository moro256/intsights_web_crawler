import json

config_path = "./intsights_web_crawler/config/config.json"


def provide(logger):
    try:
        with open(config_path) as config_file:
            data = json.load(config_file)
            return data
    except Exception as e:
        logger.log('could not read config file:', 'ERROR')
        raise e
