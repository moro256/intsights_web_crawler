from intsights_web_crawler.storage.base_storage import BaseStorage
from tinydb import TinyDB, Query
import json


class TinyDbStorage(BaseStorage):
    def __init__(self, path, logger):
        self.path = path
        self.db = TinyDB(path)
        self.logger = logger

    def write(self, rows):
        for row in rows:
            self.upsert_single(row)

    def upsert_single(self, row):
        try:
            query = Query()
            self.db.upsert(json.loads(str(row)), query.id == row.id)
        except Exception as e:
            self.logger.log("could not write to db: " + str(e), "ERROR")
