from threading import Timer


class CrawlerRunner:
    """CrawlerRunner will run the provided crawler periodically and write the crawl results to storage,
    it will also catch any exceptions
        Args:
            crawler: crawler to run
            storage: storage for storing results
            logger: logger for logging status
            interval_sec: amount of time between each crawl in seconds
        """
    def __init__(self, crawler, storage, logger, interval_sec):
        self.crawler = crawler
        self.interval_sec = interval_sec
        self.logger = logger
        self.storage = storage

    def start(self):
        """start crawling periodically and catch any errors that occur, crawl results will be written to the storage
        provided in the constructor """
        try:
            self.logger.log('starting crawl...', 'INFO')
            result = self.crawler.crawl()
            self.storage.write(result)
            self.logger.log('finished crawling interval.', 'INFO')
        except Exception as e:
            self.logger.log('could not finish crawling: ' + str(e), 'ERROR')
        Timer(self.interval_sec, self.start).start()
