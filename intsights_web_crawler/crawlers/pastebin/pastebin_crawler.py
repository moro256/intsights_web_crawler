from intsights_web_crawler.extractors.pastebin.pastebin_archive_link_extractor import PastebinArchiveLinkExtractor
from intsights_web_crawler.extractors.concurrent_extractor import ConcurrentExtractor
from intsights_web_crawler.extractors.pastebin.paste_extractor import PasteExtractor


class PastebinCrawler:
    def __init__(self, pastebin_url, pastes_page, max_workers):
        self.pastebin_url = pastebin_url
        self.pastes_page = pastes_page
        self.max_workers = max_workers

    def crawl(self):
        urls = PastebinArchiveLinkExtractor(self.pastebin_url, self.pastes_page).extract()
        paste_extractors = [PasteExtractor(url) for url in urls]
        pastes = ConcurrentExtractor(self.max_workers, paste_extractors).extract()
        return pastes
