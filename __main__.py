from intsights_web_crawler.crawlers.pastebin.pastebin_crawler import PastebinCrawler
from intsights_web_crawler.services import config_provider
from intsights_web_crawler.storage.tiny_db_storage import TinyDbStorage
from intsights_web_crawler.logger.console_logger import ConsoleLogger
from intsights_web_crawler.crawlers.crawler_runner import CrawlerRunner
import os
os.chdir(os.path.abspath(os.path.dirname(__file__)))


def main():
    logger = ConsoleLogger()
    config = config_provider.provide(logger)
    storage = TinyDbStorage(config["db_path"], logger)
    crawler = PastebinCrawler(config["pastebin_url"], config["pastes_page"], config["max_workers"])
    crawler_runner = CrawlerRunner(crawler, storage, logger, config["interval_sec"])
    crawler_runner.start()


main()
