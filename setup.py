import setuptools

setuptools.setup(
    name='intsights_web_crawler',
    version='1.0',
    author='Mor Ohana',
    packages=setuptools.find_packages(),
    python_requires='>=3.7.5',
)
